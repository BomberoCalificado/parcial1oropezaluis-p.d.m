using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CascaraBanana : MonoBehaviour
{
    public float TiempoUtil;


    // Start is called before the first frame update
    void Start()
    {
        TiempoUtil = 2;
    }

    // Update is called once per frame
    void Update()
    {
        TiempoUtil = Mathf.Clamp(TiempoUtil, -0.5f, 2);

        TiempoUtil = TiempoUtil - Time.deltaTime;

        if (TiempoUtil <= 0)
        {
            gameObject.SetActive(false); //desactivamos la cascara de banana
            
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            gameObject.SetActive(false); //desactivamos la banana
        }

        if (collision.gameObject.CompareTag("Rata"))
        {
            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("CascaraBanana"))
        {
            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("Muerte"))
        {
            gameObject.SetActive(false);
        }
    }

    void OnDisable()
    {
        // Reiniciar la variable TiempoUtil a 5 cuando el objeto se desactive
        TiempoUtil = 2;
    
    }

    void OnEnable()
    {
        // Opcional: Asegurarse de que la variable Activado est� en true cuando se active el objeto
   
    }
}
