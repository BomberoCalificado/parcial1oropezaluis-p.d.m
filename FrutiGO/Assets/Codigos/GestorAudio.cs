using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestorAudio : MonoBehaviour
{
    public static GestorAudio instancia;

    [System.Serializable]
    public class Sonido
    {
        public string nombre;
        public AudioClip clip;
        [Range(0f, 1f)] public float volumen = 1f;
        [Range(0.1f, 3f)] public float pitch = 1f;
        public bool loop = false;
    }

    public Sonido[] sonidos;

    private Dictionary<string, AudioClip> diccionarioSonidos;
    private Dictionary<string, AudioSource> diccionarioFuentes;

    private void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        diccionarioSonidos = new Dictionary<string, AudioClip>();
        diccionarioFuentes = new Dictionary<string, AudioSource>();

        foreach (Sonido sonido in sonidos)
        {
            diccionarioSonidos[sonido.nombre] = sonido.clip;

            // Crear un nuevo AudioSource para cada sonido
            AudioSource nuevaFuente = gameObject.AddComponent<AudioSource>();
            nuevaFuente.clip = sonido.clip;
            nuevaFuente.volume = sonido.volumen;
            nuevaFuente.pitch = sonido.pitch;
            nuevaFuente.loop = sonido.loop;
            nuevaFuente.playOnAwake = false;
            nuevaFuente.spatialBlend = 0f; // Asegurarse de que el audio sea 2D

            diccionarioFuentes[sonido.nombre] = nuevaFuente;
        }
    }

    // M�todo para reproducir un sonido por su nombre
    public void Reproducir(string nombre)
    {
        if (diccionarioFuentes.ContainsKey(nombre))
        {
            diccionarioFuentes[nombre].Play();
        }
        else
        {
            Debug.LogWarning("Sonido no encontrado: " + nombre);
        }
    }

    // M�todo para detener un sonido por su nombre
    public void Detener(string nombre)
    {
        if (diccionarioFuentes.ContainsKey(nombre))
        {
            diccionarioFuentes[nombre].Stop();
        }
        else
        {
            Debug.LogWarning("Sonido no encontrado: " + nombre);
        }
    }

    // M�todo p�blico para detener el audio con un retraso
    public void DetenerAudioConRetraso(string nombre, float retraso)
    {
        StartCoroutine(DetenerConRetraso(nombre, retraso));
    }

    // Corrutina para detener el audio despu�s de un retraso
    private IEnumerator DetenerConRetraso(string nombre, float retraso)
    {
        yield return new WaitForSeconds(retraso);
        Detener(nombre);        
    }

    public void Pausar(string nombre)
    {
        if (diccionarioFuentes.ContainsKey(nombre))
        {
            diccionarioFuentes[nombre].Pause();
        }
        else
        {
            Debug.LogWarning("Sonido no encontrado: " + nombre);
        }
    }

    // M�todo para reanudar un sonido por su nombre
    public void Reanudar(string nombre)
    {
        if (diccionarioFuentes.ContainsKey(nombre))
        {
            diccionarioFuentes[nombre].UnPause();
        }
        else
        {
            Debug.LogWarning("Sonido no encontrado: " + nombre);
        }
    }

}
