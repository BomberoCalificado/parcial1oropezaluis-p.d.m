using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Rendering;
//using UnityEditor.Rendering.LookDev;

// mobile

public class Jugador : MonoBehaviour
{
    [Header("ESTA VARIABLE INDICA QUE YA ESTA HABILITADO PARA JUGAR")] 
    public bool Jugar;
    [Space(3)]

    [Header("PUNTAJE")]
    public float PuntajeJugador;
    [Space(3)]

    [Header("DURACION DE ESTADOS DEL JUGADOR")]
    public float ContadorAturdido; // cuanto tiempo se aturde el jugador, tropezandose
    public float PuntosDoble; // cuanto tiempo el jugador multiplica x2 el puntos
    public float PuntosInvertidos; // cuanto tiempo el jugador multiplica x2 el puntos
    public float ContadorE5;                          // ESTADO 5 FIESTA DE MANZANAS

    [Space(3)]

    [Header("ESTADOS DEL JUGADOR")]
    public int IndicadorEstado; // indica en que estado se encuentra el jugador
                                // 0 normal
                                // 1 Mojado: el jugador se relentiza por 10 seg
                                // 2 aturdido cascara banana
                                // 3 puntos dobles
                                // 4 invertido resta puntos
                                // 5 fiesta de manzanas
                                // 6 jugador invisible

    [Header("ESTADO 1 MOJADO")]
    [SerializeField] private float ContadorMojado;
    public float VelocidadLenta;
    private float VelocidadNormal;
    private float AceleleracionNormal;
    public float AceleleracionLenta;
    // Referencia al componente SpriteRenderer del personaje
    private SpriteRenderer spriteRenderer;
    public Color Mojado;
    public Color Normal;  
    [Space(3)]

    [Header("ESTADO 6 INVISIBLE")]

    // Colores asignados desde el inspector
    public Color colorInicial; // Color inicial
    public Color color1; // Primer color del parpadeo
    public Color color2; // Segundo color del parpadeo
    // Variables para controlar la duraci�n del cambio de color
    public float duracionParpadeo; // Duraci�n total del parpadeo en segundos
    public float frecuenciaParpadeo; // Frecuencia del parpadeo (tiempo entre cambios de color)

    private Coroutine parpadeoCoroutine; // Referencia a la corrutina de parpadeo

    [Space(3)]


    [Header("MOVIMIENTO")] 
    public float FuerzaMov;
    protected Rigidbody2D rb;
    protected PlayerInput Pinput;
    protected Vector2 MiInput;
    public float FuerzaFreno = 5f;     // Cantidad de frenado
    private bool botonFrenarPresionado = false;
    public float maxAceleracion;  // Aceleraci�n m�xima para el movimiento del jugador
    private Vector3 Escala;    // ROTACION
    private float X;

    [Header("MECANICA DE SACUDIR PAREDES")] 
    public CamaraEfecto VibracionCamara;
    [SerializeField] private float Magnitud; // controla la magnitud de la sacudida de la camara
    [SerializeField] private float Duracion; // cuanto tiempo se sacude la camara
    [Space(3)]


    [Header("SALTAR")]   
    // Variable p�blica para ajustar la fuerza del salto desde el inspector
    [SerializeField] private float fuerzaSalto;
    // Variable para verificar si el jugador est� en el suelo
    private bool enSuelo;
    [Space(3)]

    [Header("ANIMACIONES")] 
    private Animator Anim;
    public bool CCorrer;
    public bool Resbalar;    
    public float ValorAcelerometro;

    // SONIDOS
    private bool Reproduciendo;



    private void Start()
    {
        ControlNivel.Ganar = false;
        IndicadorEstado = 0;
        PararAudioEventos();
        GestorAudio.instancia.Reproducir("Musica");
        Reproduciendo = false;
        spriteRenderer = GetComponent<SpriteRenderer>();
        Anim = GetComponent<Animator>();
        Jugar = true;    
        rb = GetComponent<Rigidbody2D>();
        Pinput = GetComponent<PlayerInput>();        
        rb.constraints = RigidbodyConstraints2D.FreezeRotation; // frezar rotacion
        Escala = transform.localScale;
        X = Escala.x;
        ContadorAturdido = 0;
        ContadorMojado = 0;
        PuntosDoble = 0;
        PuntosInvertidos = 0;
        maxAceleracion = 20;
        VelocidadNormal = FuerzaMov;
        AceleleracionNormal = maxAceleracion;


   
    }

    private void Update()
    {
        if (ControlNivel.Perder == true)
        {
            PararAudioEventos();
        }
              
        if (ControlNivel.Ganar == false && ControlNivel.Perder == false)
            {
            PuntajeJugador = Mathf.Clamp(PuntajeJugador, 0, 1000);

            if (IndicadorEstado != 2)
                {
                    DireccionJugador();
                }

            LogicaEstados();

            // Aplicar frenado si el bot�n de freno est� presionado
            if (botonFrenarPresionado)
            {
                AplicarFrenado();
            }

         
        }
        Enlace();
      
        if (Reproduciendo)
        {
            GestorAudio.instancia.Pausar("Musica");
        }
        else { GestorAudio.instancia.Reanudar("Musica"); }

    }

    private void FixedUpdate()
    {
        if (ControlNivel.Ganar == false && ControlNivel.Perder == false)
        {
            ValorAcelerometro = Input.acceleration.x;

            if (Input.acceleration.x > 0.10f || Input.acceleration.x < -0.10f)
            {
                CCorrer = true;
            }
            else
            {
                CCorrer = false;
            }


            // Aplicar fuerza al rigidbody basado en la aceleraci�n del aceler�metro
            Vector3 movimiento = new Vector3(Input.acceleration.x, 0, 0);
            rb.AddForce(movimiento * FuerzaMov);

            // Limitar la velocidad m�xima del jugador para evitar movimientos demasiado r�pidos
           if (rb.velocity.magnitude > maxAceleracion)
           {
              rb.velocity = rb.velocity.normalized * maxAceleracion;
           }
            
        }

    }

    public void DireccionJugador()
    {

       // Actualizar la direcci�n del jugador basada en la aceleraci�n del aceler�metro
        float movimientoHorizontal = Input.acceleration.x;

        // Cambiar la escala del jugador dependiendo de la direcci�n del movimiento
        if (movimientoHorizontal > 0)
        {
            transform.localScale = new Vector3(-X, Escala.y, Escala.z);
     
        }
        else if (movimientoHorizontal < 0)
        {
            transform.localScale = new Vector3(X, Escala.y, Escala.z);
           
        }
      
    }

    public void Frenar(InputAction.CallbackContext contexto)
    {
        if (contexto.started)
        {
            botonFrenarPresionado = true;
        }

        if (contexto.canceled)
        {
            botonFrenarPresionado = false;
        }

    }

    public void AplicarFrenado()
    {
        // Reducir gradualmente la velocidad del jugador multiplicando la velocidad actual por un factor de frenado
        rb.velocity *= (1 - FuerzaFreno * Time.deltaTime);
    }

    // M�todo para activar el freno desde el bot�n del Canvas
    public void ActivarFreno()
    {
        botonFrenarPresionado = true;
    }

    // M�todo para desactivar el freno desde el bot�n del Canvas
    public void DesactivarFreno()
    {
        botonFrenarPresionado = false;
    }    
   
    private void LogicaEstados()
    {


        ContadorAturdido = Mathf.Clamp(ContadorAturdido, 0, 2);
        ContadorMojado = Mathf.Clamp(ContadorMojado, 0, 6);
        // Evaluar el valor de IndicadorEstado usando un switch
        switch (IndicadorEstado)
        {
            // normal
            case 0:
                // Acci�n para el estado 0 es normal y frena todos los audios de evento

                if (Reproduciendo == true)
                {
                    GestorAudio.instancia.Detener("FiestaPuntos");
                    GestorAudio.instancia.Detener("Horror");
                    GestorAudio.instancia.Detener("Frio");
                    GestorAudio.instancia.DetenerAudioConRetraso("ManzanaFiesta", 1);
                    Reproduciendo = false;
                }
             
                break;

            // Mojado: el jugador se relentiza por 8 seg
            case 1:


                if (IndicadorEstado == 1)
                {
                    maxAceleracion = AceleleracionLenta;
                    FuerzaMov = VelocidadLenta; // se relentiza al jugador
                    ContadorMojado = ContadorMojado - Time.deltaTime; // descontar hasta 0


                    if (ContadorMojado <= 0)
                    {
                        maxAceleracion = AceleleracionNormal;
                        FuerzaMov = VelocidadNormal;
                        spriteRenderer.color = Normal;
                        IndicadorEstado = 0;
                        GestorAudio.instancia.Detener("Frio");
                    }

                }


                break;

            // CASCARA BANANA aturdido 2 segundos - 10 puntos al caer
            case 2:


                if (IndicadorEstado == 2)
                {

                    // Congelar posici�n en X e Y, pero mantener la congelaci�n de la rotaci�n en Z
                    rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;

                    ContadorAturdido = ContadorAturdido - Time.deltaTime; // descontar hasta 0


                    if (ContadorAturdido <= 0)
                    {
                        IndicadorEstado = 0;
                    }
                }

                if (IndicadorEstado != 2)
                {

                    // Restablecer la posici�n en X e Y, pero mantener la congelaci�n de la rotaci�n en Z
                    rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;
                }
                break;

            // Multiplica los puntos a recibir x2 por 10 seg y suma 10 puntos
            case 3:

                PuntosDoble = Mathf.Clamp(PuntosDoble, 0, 8);

                PuntosDoble = PuntosDoble - Time.deltaTime; // empieza el conteo regresivo



                if (PuntosDoble <= 0)
                {
                    IndicadorEstado = 0; // cambia a estado normal 0
                }




                break;

            // resta los puntos a recibir frutas por 10 seg.
            case 4:

                PuntosInvertidos = Mathf.Clamp(PuntosInvertidos, 0, 10);

                PuntosInvertidos = PuntosInvertidos - Time.deltaTime; // empieza el conteo regresivo

               
                if (PuntosInvertidos <= 0)
                {
                   
                    IndicadorEstado = 0; // cambia a estado normal 0
                }

                break;

            // fiesta de manzanas por x tiempo
            case 5:

                ContadorE5 = Mathf.Clamp(ContadorE5, 0, 10);

                ContadorE5 = ContadorE5 - Time.deltaTime; // empieza el conteo regresivo

                if (ContadorE5 <= 0)
                {
                    IndicadorEstado = 0; // cambia a estado normal 0
                }

                break;

            default:
                // Acci�n para cualquier otro valor no especificado
              
                break;
        }


        

    }

    public void VolverseInvisible()
    {
        // Iniciar la corrutina para cambiar el color temporalmente
        ControlNivel.ActivarInvisible = true;
        IniciarParpadeo();
    }

    public void IniciarParpadeo()
    {
        // Si ya hay una corrutina de parpadeo activa, la detenemos
        if (parpadeoCoroutine != null)
        {
            StopCoroutine(parpadeoCoroutine);
        }

        // Iniciamos la nueva corrutina de parpadeo
        parpadeoCoroutine = StartCoroutine(Parpadear());
    }

    public void DetenerParpadeo()
    {
        // Si la corrutina de parpadeo est� activa, la detenemos
        if (parpadeoCoroutine != null)
        {
            StopCoroutine(parpadeoCoroutine);
            parpadeoCoroutine = null;
        }

        // Restablecemos el color inicial
        spriteRenderer.color = colorInicial;
    }

    private IEnumerator Parpadear()
    {
        float tiempoTranscurrido = 0f;

            // pasar a color 2
            spriteRenderer.color = color2;

            // Esperar un tiempo igual a la frecuencia del parpadeo
            yield return new WaitForSeconds(duracionParpadeo);

            // Incrementar el tiempo transcurrido
            tiempoTranscurrido += duracionParpadeo;
       

        // Restablecer el color inicial despu�s del parpadeo
        spriteRenderer.color = colorInicial;
    }

    private void Enlace()
    {
        if (IndicadorEstado == 2)
        {
            Resbalar = true;
        }
        else { Resbalar = false; }


        Anim.SetBool("Corriendo", CCorrer);
        Anim.SetBool("Tropezar", Resbalar);
        Anim.SetBool("Ganador", ControlNivel.Ganar);
    }

    public void Saltar()
    {
        // Verifica si el jugador est� en el suelo para permitir el salto
        if (enSuelo == true && IndicadorEstado != 2 && !ControlNivel.Perder && !ControlNivel.Ganar)
        {
            // Aplicar una fuerza de salto en el eje Y
            rb.AddForce(new Vector2(0, fuerzaSalto), ForceMode2D.Impulse);
            enSuelo = false; // El jugador ya no est� en el suelo despu�s de saltar
        }
    }
    
    public void PararAudioEventos()
    {
        GestorAudio.instancia.Detener("Musica");
        GestorAudio.instancia.Detener("FiestaPuntos");
        GestorAudio.instancia.Detener("Horror");
        GestorAudio.instancia.Detener("ManzanaFiesta");
        GestorAudio.instancia.Detener("Frio");
    }    

    public void VibrarCamara(float tiempo, float magnitud)
    {
        // Inicia la rutina de vibraci�n de la c�mara con una duraci�n de 0.5 segundos y una magnitud de 0.3
        StartCoroutine(VibracionCamara.Vibrar(tiempo, magnitud));
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Si el jugador colisiona con el suelo, permite saltar de nuevo
        if (collision.gameObject.CompareTag("Suelo") && !ControlNivel.Ganar)
        {
            enSuelo = true;
        }

        if (collision.gameObject.CompareTag("Pocion") && !ControlNivel.Ganar)
        {
            if (IndicadorEstado == 0)
            {
                VolverseInvisible();
            }
            
        }

        if (collision.gameObject.CompareTag("Muerte") && !ControlNivel.Ganar)
        {
            ControlNivel.Perder = true;
        }

        // Suma 30 puntos, si cae al suelo queda la cascara la cual hace que el jugador se aturda 2 seg si la pisa, sino desaparece en 2 seg.
        if (collision.gameObject.CompareTag("Banana") && !ControlNivel.Ganar)
        {
            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 30;

                    break;

                // normal
                case 1:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 30;

                    break;

                // no recibe nada
                case 2:

                    // nada

                    break;

                // suma x 2
                case 3:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 60;

                    break;

                // solo resta 20
                case 4:
                    GestorAudio.instancia.Reproducir("Restar");
                    PuntajeJugador = PuntajeJugador - 30;

                    break;

                case 5:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 30;

                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }

        }

        // Aturdir al jugador si pisa una cascara de banana y resta 10 puntos
        if (collision.gameObject.CompareTag("CascaraBanana") && !ControlNivel.Ganar)
        {
            
            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    DetenerParpadeo();
                    GestorAudio.instancia.Reproducir("Caida");                   
                    Handheld.Vibrate();
                    IndicadorEstado = 2;
                    ContadorAturdido = 2;
                    PuntajeJugador = PuntajeJugador - 20;

                    break;


                case 1:
                    // desactiva mojado
                    DetenerParpadeo();
                    ContadorMojado = 0;
                    maxAceleracion = AceleleracionNormal;
                    FuerzaMov = VelocidadNormal;
                    spriteRenderer.color = Normal;
                    GestorAudio.instancia.Detener("Frio");

                    // hace lo propio
                    GestorAudio.instancia.Reproducir("Caida");
                    Handheld.Vibrate();
                    IndicadorEstado = 2;
                    ContadorAturdido = 2;
                    PuntajeJugador = PuntajeJugador - 20;

                    break;


                case 2:

                    // desactiva aturdido
                    ContadorAturdido = 0;
                    // Restablecer la posici�n en X e Y, pero mantener la congelaci�n de la rotaci�n en Z
                    rb.constraints = RigidbodyConstraints2D.None | RigidbodyConstraints2D.FreezeRotation;

                    // hace lo propio
                    Handheld.Vibrate();
                    IndicadorEstado = 2;
                    ContadorAturdido = 2;
                    PuntajeJugador = PuntajeJugador - 20;

                    break;

                // solo suma 20
                case 3:

                    // nada

                    break;

                // solo resta 10
                case 4:

                    // nada

                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }

        }

        if (collision.gameObject.CompareTag("Balde") && !ControlNivel.Ganar)
        {

            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    DetenerParpadeo();
                    GestorAudio.instancia.Reproducir("Agua");
                    GestorAudio.instancia.Reproducir("Frio");                    
                    IndicadorEstado = 1;
                    ContadorMojado = 6;              
                    spriteRenderer.color = Mojado;

                    break;

               
                case 1:

                    // nada

                    break;

                
                case 2:

                    // nada

                    break;

                // solo suma 20
                case 3:

                   

                    break;

                // solo resta 10
                case 4:

                    

                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }
        }

        if (collision.gameObject.CompareTag("Pared") && !ControlNivel.Ganar)
        {
         GestorAudio.instancia.Reproducir("Pared");
         Handheld.Vibrate();
         
         VibrarCamara(Duracion,Magnitud);

        }

        // Suma 20 puntos
        if (collision.gameObject.CompareTag("Manzana"))
        {
            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 20;
                  

                    break;

                // normal
                case 1:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 20;

                    break;

                // no recibe nada
                case 2:

                    // nada

                    break;

                // suma x 2
                case 3:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 40;

                    break;

                // solo resta 20
                case 4:
                    GestorAudio.instancia.Reproducir("Restar");
                    PuntajeJugador = PuntajeJugador - 20;

                    break;
                // normal

                case 5:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 20;


                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }

        }

        if (collision.gameObject.CompareTag("Naranja") && !ControlNivel.Ganar)
        {
            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 10;


                    break;

                // normal
                case 1:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 10;

                    break;

                // no recibe nada
                case 2:

                    // nada

                    break;

                // suma x 2
                case 3:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 20;

                    break;

                // solo resta 20
                case 4:
                    GestorAudio.instancia.Reproducir("Restar");
                    PuntajeJugador = PuntajeJugador - 10;

                    break;

                case 5:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 10;


                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }

        }

        // Multiplica los puntos x2 por 10 seg y suma 10 puntos
        if (collision.gameObject.CompareTag("NaranjaLoca") && !ControlNivel.Ganar)
        {
            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    DetenerParpadeo();
                    GestorAudio.instancia.Reproducir("FiestaPuntos");
                    Reproduciendo = true;
                    PuntajeJugador = PuntajeJugador + 10;
                    IndicadorEstado = 3;
                    PuntosDoble = 8;

                    break;

                // normal
                case 1:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 10;
                    //IndicadorEstado = 3;
                   // PuntosDoble = 5;
                    break;

                // no recibe nada
                case 2:

                    // nada

                    break;

                // solo suma 20
                case 3:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 20;

                    break;

                // solo resta 10
                case 4:
                    GestorAudio.instancia.Reproducir("Restar");
                    PuntajeJugador = PuntajeJugador - 10;

                    break;

                case 5:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 10;


                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }

        }

        //  Aplica un estado invertido y resta puntos por cada fruta que atrapes por 10 seg
        if (collision.gameObject.CompareTag("Rata") && !ControlNivel.Ganar)
        {

            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    DetenerParpadeo();
                    GestorAudio.instancia.Reproducir("Horror");
                    Reproduciendo = true;
                    PuntajeJugador = PuntajeJugador - 10;
                    IndicadorEstado = 4;
                    PuntosInvertidos = 8;

                    break;

                // normal
                case 1:
                    DetenerParpadeo();
                    // desactiva mojado
                    maxAceleracion = AceleleracionNormal;
                    FuerzaMov = VelocidadNormal;
                    spriteRenderer.color = Normal;
                    GestorAudio.instancia.Detener("Frio");

                    // hace lo propio
                    GestorAudio.instancia.Reproducir("Horror");
                    Reproduciendo = true;
                    ContadorMojado = 0;
                    PuntajeJugador = PuntajeJugador - 10;
                    IndicadorEstado = 4;
                    PuntosInvertidos = 5;

                    break;

                // no recibe nada
                case 2:

                    // nada

                    break;

                // no tiene efecto
                case 3:

                // no tiene efecto

                    break;

                // solo resta 10
                case 4:

                    PuntajeJugador = PuntajeJugador - 10;

                    break;

                case 5:

                    PuntajeJugador = PuntajeJugador - 10;

                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }
           
        }

        if (collision.gameObject.CompareTag("Sandia") && !ControlNivel.Ganar)
        {
            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 50;

                    break;

                // normal
                case 1:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 50;

                    break;

                // no recibe nada
                case 2:

                    // nada
                 
                    break;

                // Multiplica  x2 suma 100;
                case 3:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 100;

                    break;

                // resta 
                case 4:
                    GestorAudio.instancia.Reproducir("Restar");
                    PuntajeJugador = PuntajeJugador - 50;
                    
                    break;

                case 5:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 50;

                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }
            }

        if (collision.gameObject.CompareTag("Uva") && !ControlNivel.Ganar)
        {
            switch (IndicadorEstado)
            {
                // normal
                case 0:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 30;


                    break;

                // normal
                case 1:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 30;

                    break;

                // no recibe nada
                case 2:

                    // nada

                    break;

                // suma x 2
                case 3:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 60;

                    break;

                // solo resta 20
                case 4:
                    GestorAudio.instancia.Reproducir("Restar");
                    PuntajeJugador = PuntajeJugador - 30;

                    break;

                // normal
                case 5:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 30;


                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }

        }

        if (collision.gameObject.CompareTag("ManzanaLoca"))
        {
            switch (IndicadorEstado)
            {
                // normal activa evento estado 5
                case 0:
                    DetenerParpadeo();
                    GestorAudio.instancia.Reproducir("ManzanaFiesta");
                    Reproduciendo = true;
                    ContadorE5 = 10;
                    IndicadorEstado = 5;
                    PuntajeJugador = PuntajeJugador + 20;                   
                  
                    break;

                // solo recibe puntos
                case 1:
                    
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 20;
                   
                    break;

                // no recibe nada
                case 2:

                    // nada

                    break;

                // solo suma 20
                case 3:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 20;

                    break;

                // solo resta 20
                case 4:
                    GestorAudio.instancia.Reproducir("Restar");
                    PuntajeJugador = PuntajeJugador - 20;

                    break;

                // normal activa evento estado 5
                case 5:
                    GestorAudio.instancia.Reproducir("Sumar");
                    PuntajeJugador = PuntajeJugador + 20;
                   
                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }

        }

        if (collision.gameObject.CompareTag("Reloj") && !ControlNivel.Ganar)
        {
            switch (IndicadorEstado)
            {
                // normal activa evento estado 5
                case 0:
                    GestorAudio.instancia.Reproducir("Reloj");
                    ControlNivel.TiempoActual = ControlNivel.TiempoActual + 10;

                    break;

                // solo recibe puntos
                case 1:
                    GestorAudio.instancia.Reproducir("Reloj");
                    ControlNivel.TiempoActual = ControlNivel.TiempoActual + 10;

                    break;

                // no recibe nada
                case 2:

                    // nada

                    break;

                // solo suma 20
                case 3:
                    GestorAudio.instancia.Reproducir("Reloj");
                    ControlNivel.TiempoActual = ControlNivel.TiempoActual + 20;

                    break;

                // solo resta
                case 4:
                    GestorAudio.instancia.Reproducir("Restar");
                    ControlNivel.TiempoActual = ControlNivel.TiempoActual - 10;

                    break;

               
                case 5:

                    // normal activa evento estado 5

                    GestorAudio.instancia.Reproducir("Reloj");
                    ControlNivel.TiempoActual = ControlNivel.TiempoActual + 10;

                  

                    break;

                default:
                    // Acci�n para cualquier otro valor no especificado

                    break;
            }

        }

    }

   



}
