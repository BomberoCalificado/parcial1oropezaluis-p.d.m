using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruta : MonoBehaviour
{
    private Rigidbody2D rb;
    public float fuerzaContraria = 3.0f;
    public float velocidadMaxima = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
      //  rb.gravityScale = gravedadEscala;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void FixedUpdate()
    {
        // Aplicar una fuerza contraria a la gravedad
        rb.AddForce(Vector2.up * fuerzaContraria);

        // Limitar la velocidad m�xima de ca�da
        if (rb.velocity.y < -velocidadMaxima)
        {
            rb.velocity = new Vector2(rb.velocity.x, -velocidadMaxima);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            gameObject.SetActive(false);
        }
        if (collision.gameObject.CompareTag("Suelo"))
        {
            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("CascaraBanana"))
        {
            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("Pajaro"))
        {
            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("Muerte"))
        {
            gameObject.SetActive(false);
        }
    }
}
