using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetPoolSimple : MonoBehaviour
{
    // Lista de objetos que se utilizar�n en la piscina, asignada desde el Inspector
    public List<GameObject> objetosEnLaPiscina;

    private void Awake()
    {
        // Inicializa la lista de la piscina de objetos
        if (objetosEnLaPiscina == null)
        {
            objetosEnLaPiscina = new List<GameObject>();
        }

        // Desactiva todos los objetos iniciales
        foreach (GameObject obj in objetosEnLaPiscina)
        {
            obj.SetActive(false);
        }
    }

    public GameObject ObtenerObjetoAleatorio()
    {
        // Lista temporal para almacenar los objetos desactivados
        List<GameObject> objetosDesactivados = new List<GameObject>();

        // Recorre la piscina buscando objetos desactivados y los agrega a la lista temporal
        foreach (GameObject obj in objetosEnLaPiscina)
        {
            if (!obj.activeInHierarchy)
            {
                objetosDesactivados.Add(obj);
            }
        }

        // Si hay objetos desactivados disponibles
        if (objetosDesactivados.Count > 0)
        {
            // Selecciona un objeto desactivado aleatorio de la lista temporal
            int indiceAleatorio = Random.Range(0, objetosDesactivados.Count);
            GameObject objetoSeleccionado = objetosDesactivados[indiceAleatorio];
            // Activa el objeto seleccionado y lo devuelve
            objetoSeleccionado.SetActive(true);
            return objetoSeleccionado;
        }

        // Si no hay objetos desactivados disponibles, retorna null o maneja la situaci�n de otra manera
        Debug.LogWarning("No hay objetos disponibles en la piscina.");
        return null;
    }
}