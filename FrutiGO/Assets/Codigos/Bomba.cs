using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba : MonoBehaviour
{
    [Header("PULSACIONES DE ESCALA")]
    public float frecuencia = 1f; // Frecuencia de la pulsaci�n
    public float amplitud = 0.1f; // Amplitud de la pulsaci�n
    private Vector3 escalaInicial; // Escala inicial del objeto
    [Space(5)]

    [Header("FUERZA DE GRAVEDAD CONTRARIA")] 
    private Rigidbody2D rb;
    public float fuerzaContraria = 3.0f;
    public float velocidadMaxima = 5.0f;
    [Space(5)]

    [Header("REBOTE HACIA ARRIBA")]
    public float fuerzaImpulso; // Variable p�blica para la fuerza del impulso
    [Space(5)]

    [Header("EXPLOSION")]
    public float Contador;
    private bool audioReproducido = false; // Variable para controlar si el audio ya se ha reproducido
    public ParticleSystem ExplosionEfecto;

    void Start()
    {

        Contador = 5;

        rb = GetComponent<Rigidbody2D>();

        // Guardar la escala inicial del objeto
        escalaInicial = transform.localScale;

        // Buscar el objeto hijo llamado "Explosion" y obtener su componente ParticleSystem
        Transform explosionTransform = transform.Find("Explosion");
        if (explosionTransform != null)
        {
            ExplosionEfecto = explosionTransform.GetComponent<ParticleSystem>();
            if (ExplosionEfecto == null)
            {
                Debug.LogWarning("El objeto hijo 'Explosion' no tiene un componente ParticleSystem.");
            }
        }
        else
        {
            Debug.LogWarning("No se encontr� el objeto hijo 'Explosion'.");
        }

    }

    void OnEnable()
    {
        // Reiniciar la variable cuando el objeto se active nuevamente
        audioReproducido = false;
        Contador = 5;
    }

    void Update()
    {
        BombeoEscala();
        GravedadContraria();

        Contador = Contador - Time.deltaTime;

        if (Contador <= 0)
        {
            // Iniciar la corrutina para desactivar el objeto despu�s de un retardo           
            StartCoroutine(DesactivarConRetardo());
        }

     
    }

    private IEnumerator DesactivarConRetardo()
    {
        if (!audioReproducido)
        {

            // Activar las part�culas y reproducir el sonido una vez
            Handheld.Vibrate();
            ExplosionEfecto.Play();
            GestorAudio.instancia.Reproducir("Pared");
            audioReproducido = true; // Marcar el audio como reproducido

        }
                  
        // Esperar 2 segundos
        yield return new WaitForSeconds(0.55f);

        // Desactivar el objeto
        gameObject.SetActive(false);
    }

    public void BombeoEscala()
    {
        // Calcular la escala en funci�n del tiempo, la frecuencia y la amplitud
        float escala = 1 + Mathf.Sin(Time.time * frecuencia) * amplitud;

        // Aplicar la nueva escala al objeto
        transform.localScale = escalaInicial * escala;
    }

    public void GravedadContraria()
    {
        // Aplicar una fuerza contraria a la gravedad
        rb.AddForce(Vector2.up * fuerzaContraria);

        // Limitar la velocidad m�xima de ca�da
        if (rb.velocity.y < -velocidadMaxima)
        {
            rb.velocity = new Vector2(rb.velocity.x, -velocidadMaxima);
        }
    }

    public void AplicarImpulso()
    {
        if (rb != null)
        {
            // Aplicar una fuerza hacia arriba
            rb.AddForce(Vector2.up * fuerzaImpulso, ForceMode2D.Impulse);
        }
        else
        {
            Debug.LogWarning("Rigidbody2D no est� asignado.");
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Si el jugador colisiona con el suelo, permite saltar de nuevo
        if (collision.gameObject.CompareTag("Suelo"))
        {
            // Iniciar la corrutina para desactivar el objeto despu�s de un retardo
            GestorAudio.instancia.Reproducir("Pared");
            StartCoroutine(DesactivarConRetardo());
        }

        // Si el jugador colisiona con el suelo, permite saltar de nuevo
        if (collision.gameObject.CompareTag("Jugador"))
        {
            GestorAudio.instancia.Reproducir("Rebotar");
            AplicarImpulso();
        }

        if (collision.gameObject.CompareTag("Muerte"))
        {
            // Iniciar la corrutina para desactivar el objeto despu�s de un retardo
            GestorAudio.instancia.Reproducir("Pared");
            StartCoroutine(DesactivarConRetardo());
        }
    }

    }
