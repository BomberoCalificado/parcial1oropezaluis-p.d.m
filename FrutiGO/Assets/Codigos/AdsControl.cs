using System;
using GoogleMobileAds.Api;
using UnityEngine;

public class ControlAnuncios : MonoBehaviour
{
    private InterstitialAd anuncioIntersticial;
    private bool puedeMostrarAd = false;

    // Cambia este ID de anuncio con tu propio ID de anuncio intersticial de AdMob
    private string idAnuncioIntersticial = "ca-app-pub-8375668984106266/3645062814";

    void Start()
    {
        // Inicializa el SDK de Google Mobile Ads.
        MobileAds.Initialize(initStatus => { });


    }

    // M�todo para mostrar el anuncio intersticial
    public void MostrarAnuncioIntersticial()
    {
        if (puedeMostrarAd)
        {
            anuncioIntersticial.Show();
            puedeMostrarAd = false;
        }
    }

    // Maneja el evento de carga completa del anuncio
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        Debug.Log("Anuncio intersticial cargado correctamente.");
        puedeMostrarAd = true;
    }
}