using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tierra : MonoBehaviour
{
    public Sprite Tierra100; // El nuevo sprite que se va a asignar
    public Sprite Tierra50; // El nuevo sprite que se va a asignar
    private SpriteRenderer spriteRenderer; // Referencia al componente SpriteRenderer
    public float Vida;


    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        Vida = 100;
    }

    // Este m�todo se llama cada vez que el objeto se activa
    void OnEnable()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        // spriteRenderer.sprite = Tierra100; // 
        Vida = 100; // Restablece la vida a 100
    }

    void Update()
    {

        Comportamiento();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Si el jugador colisiona con el suelo, permite saltar de nuevo
        if (collision.gameObject.CompareTag("Bomba"))
        {
            Vida = Vida - 50;
        }

    }

    private void Comportamiento()
    {

        if (Vida == 50) 
        {
            spriteRenderer.sprite = Tierra50;
        }

        if (Vida == 0)
        {
            gameObject.SetActive(false);
        }
    }
}
