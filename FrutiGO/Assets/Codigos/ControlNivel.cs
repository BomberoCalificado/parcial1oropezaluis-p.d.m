using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using Unity.Mathematics;
using UnityEngine.Rendering.Universal;

public class ControlNivel : MonoBehaviour
{
    public static bool Ganar;
    public static bool Perder;

    [Header("CICLO DE JUEGO HUD")]
    public float ObjetivoPuntos;
    public Jugador MiJugador;
    public static float TiempoActual;
    public float TiempoLimite;
    public static int Nivel;
    [Space(5)]

    // HUD
    [Header("CICLO DE JUEGO HUD")]
    public TextMeshProUGUI TextPuntajeActual;
    public TextMeshProUGUI TextObjetivo;
    public TextMeshProUGUI TextTiempoDisponible;
    public GameObject CartelGanar;
    public GameObject CartelPerder;
    [Space(5)]

    [Header("MENU PAUSA HUD")]
    public GameObject BotonContinuar;
    public GameObject BotonReiniciar;
    public GameObject BotonMenu;
    public GameObject BotonSalir;
    [Space(5)]

    [Header("ESTADOS DE JUGADOR HUD")]
    public GameObject MojadoHUD;
    public GameObject AturdidoHUD;
    public GameObject PuntosDoblesHUD;
    public GameObject TodoRestaHUD;
    public GameObject FiestaManzanaHUD;
    public GameObject InvisibleHUD;

    [Space(5)]

    [Header("HUD INVISIBLE")]
   
    public Color color1; // Primer color del parpadeo
    public Color color2; // Segundo color del parpadeo
    public float duracionParpadeo; // Duraci�n total del parpadeo en segundos
    public float frecuenciaParpadeo; // Frecuencia del parpadeo (tiempo entre cambios de color)

    private SpriteRenderer spriteRenderer; // Referencia al componente SpriteRenderer
    private Coroutine parpadeoCoroutine; // Referencia a la corrutina de parpadeo

    public static bool ActivarInvisible = false; // Indica si el parpadeo est� activo


    [Space(5)]
    // LUZ GLOBAL Y DE TIEMPO REAL

    [Header("LUCES EVENTOS JUGADOR")]
    public Color Blanco;
    public Color Rojo;
    public Light2D LuzGlobal;
    // Corrutina de cambio de color
    private Coroutine colorChangeCoroutine;

    [Header("CONFIGURACION DEL NIVEL")]


    //MENU DE PAUSA
    private bool juegoPausado = false; // Indica si el juego est� pausado
  

    void Start()
    {
        Time.timeScale = 1f;
        // Obtener el componente SpriteRenderer del objeto InvisibleHUD
        spriteRenderer = InvisibleHUD.GetComponent<SpriteRenderer>();
        spriteRenderer.color = color1;
        CartelGanar.SetActive(false);
        CartelPerder.SetActive(false);
        BotonContinuar.SetActive(false);
        BotonReiniciar.SetActive(false);
        BotonMenu.SetActive(false);
        BotonSalir.SetActive(false);
        Ganar = false;
        Perder = false;

      
       MiJugador.PuntajeJugador = 0;
       
       TiempoLimite = 0;
       
      
        ElegirNivel();

    }


    void Update()
    {


        CicloJuego();

        if (Perder == true)
        {
            // MOSTRAR CARTEL MENCIONANDO QUE PERDISTE y BOTON REINICIAR NIVEL ACTUAL O VOLVER AL MENU O SALIR DEL JUEGO
            PararAudioEventos();
            CartelPerder.SetActive(true);
            BotonReiniciar.SetActive(true);
            BotonSalir.SetActive(true);
            //Time.timeScale = 0f;
            // FRENAR JUEGO
        }

        if (Ganar == true)
        {
            // MOSTRAR CARTEL MENCIONANDO QUE GANASTE y BOTON PASAR A SIGUIENTE NIVEL O VOLVER AL MENU O SALIR DEL JUEGO
            PararAudioEventos();
            CartelGanar.SetActive(true);
            BotonReiniciar.SetActive(true);
            BotonMenu.SetActive(true);
            BotonSalir.SetActive(true);

        }

        ControlLuces();
        EstadosJugadorHud();
        ActualizarTextos();
        IniciarParpadeo();

        if (MiJugador.IndicadorEstado >= 1)
        {
            DetenerParpadeo();
        }
    }

    // elegir dificultad
    private void ElegirNivel()
    {
        // Verificar si la escena actual es "Nivel1"
        if (SceneManager.GetActiveScene().name == "Nivel1")
        {
            Debug.Log("Nivel 1: F�cil");

            ObjetivoPuntos = 300;
            TiempoActual = 100;

            Debug.Log("Estamos en el Nivel 1");
        }

        if (SceneManager.GetActiveScene().name == "Nivel2")
        {
            ObjetivoPuntos = 400;
            TiempoActual = 90;

            Debug.Log("Estamos en el Nivel 2");
        }

        if (SceneManager.GetActiveScene().name == "Nivel3")
        {
            ObjetivoPuntos = 600;
            TiempoActual = 80;

            Debug.Log("Estamos en el Nivel 3");
        }
    }

    private void CicloJuego()
    {
        
            TiempoActual = Mathf.Clamp(TiempoActual, 0, 1000);
            TiempoActual = TiempoActual - Time.deltaTime;

            // condicion de victoria
        if (MiJugador.PuntajeJugador >= ObjetivoPuntos)


            {

                Ganar = true;

            }
           
          // condicion de derrota
        if (TiempoActual <= TiempoLimite && MiJugador.PuntajeJugador < ObjetivoPuntos && Ganar == false)
            {
                Perder = true;
            }

       
    }

    private void ControlLuces()
    {
        if (MiJugador.IndicadorEstado == 4)
        {
            LuzGlobal.color = Rojo;
            //  LuzPunto1.color = Rojo;
            // luzPunto2.color = Rojo;

           

        }
        else 
        { 
            LuzGlobal.color = Blanco;
            // LuzPunto1.color = Blanco;
            //luzPunto2.color = Blanco;

        }

        if (MiJugador.IndicadorEstado == 3)
        {

            // Iniciar la corrutina de cambio de color si no est� ya en ejecuci�n
            if (colorChangeCoroutine == null)
            {
                colorChangeCoroutine = StartCoroutine(ChangeColorOverTime());
            }


        }
        else
        {
            // Detener la corrutina de cambio de color si est� en ejecuci�n
            if (colorChangeCoroutine != null)
            {
                StopCoroutine(colorChangeCoroutine);
                colorChangeCoroutine = null;
                // Restablecer el color de la luz al color inicial
                SetGlobalLightColor(Blanco);

            }

        }

    }

    public void IniciarParpadeo()
    {
        if (ActivarInvisible == true)
        {
            // Si ya hay una corrutina de parpadeo activa, la detenemos
            if (parpadeoCoroutine != null)
            {
                StopCoroutine(parpadeoCoroutine);
            }

            // Iniciamos la nueva corrutina de parpadeo
            parpadeoCoroutine = StartCoroutine(Parpadear());

            // Desactivamos la bandera para que no se active repetidamente
            ActivarInvisible = false;
        }
    }

    private IEnumerator Parpadear()
    {
        float tiempoTranscurrido = 0f;

        while (tiempoTranscurrido < duracionParpadeo)
        {
            // Alternar entre color1 y color2
            spriteRenderer.color = (spriteRenderer.color == color1) ? color2 : color1;

            // Esperar un tiempo igual a la frecuencia del parpadeo
            yield return new WaitForSeconds(frecuenciaParpadeo);

            // Incrementar el tiempo transcurrido
            tiempoTranscurrido += frecuenciaParpadeo;
        }

        // Restablecer el color inicial despu�s del parpadeo
        spriteRenderer.color = color1;
    }

    public void DetenerParpadeo()
    {
        // Si la corrutina de parpadeo est� activa, la detenemos
        if (parpadeoCoroutine != null)
        {
            StopCoroutine(parpadeoCoroutine);
            parpadeoCoroutine = null;
        }

        // Restablecemos el color inicial
        spriteRenderer.color = color1;
    }


    private void EstadosJugadorHud()
    {
       
    if (MiJugador.IndicadorEstado == 1)
       {
          MojadoHUD.SetActive(true);
        }
    else { MojadoHUD.SetActive(false); }

        if (MiJugador.IndicadorEstado == 2)
        {
            AturdidoHUD.SetActive(true);
        }
        else { AturdidoHUD.SetActive(false); }

        if (MiJugador.IndicadorEstado == 3)
        {
            PuntosDoblesHUD.SetActive(true);
        }
        else { PuntosDoblesHUD.SetActive(false); }


        if (MiJugador.IndicadorEstado == 4)
        {
            TodoRestaHUD.SetActive(true);
        }
        else { TodoRestaHUD.SetActive(false); }


        if (MiJugador.IndicadorEstado == 5)
        {
            FiestaManzanaHUD.SetActive(true);
        }
        else { FiestaManzanaHUD.SetActive(false); }

    }

    // M�todo para establecer el color de la luz global
    public void SetGlobalLightColor(Color newColor)
    {
        if (LuzGlobal != null)
        {
            LuzGlobal.color = newColor;
        }
    }

    // Corrutina para cambiar el color de la luz global con el tiempo
    private IEnumerator ChangeColorOverTime()
    {
        while (true)
        {
            float time = 0f;

            while (time < 1f)
            {
                float h = Mathf.PingPong(time * 2f, 1f); // Usar PingPong para oscilar entre 0 y 1
                Color newColor = Color.HSVToRGB(h, 1f, 1f);
                SetGlobalLightColor(newColor);
                time += Time.deltaTime;
                yield return null;
            }
        }
    }

    private void ActualizarTextos()
    {
        TextPuntajeActual.text = "PUNTAJE " + MiJugador.PuntajeJugador.ToString();
        TextObjetivo.text = "OBJETIVO " + ObjetivoPuntos.ToString();
        TextTiempoDisponible.text = "TIEMPO " + TiempoActual.ToString("00");

    }

    public void CargarEscenaMenu()
    {
        GestorAudio.instancia.Detener("Musica");
        SceneManager.LoadScene("Menu");
    }

    public void PararAudioEventos()
    {
        GestorAudio.instancia.Detener("Musica");
        GestorAudio.instancia.Detener("FiestaPuntos");
        GestorAudio.instancia.Detener("Horror");
        GestorAudio.instancia.Detener("ManzanaFiesta");
        GestorAudio.instancia.Detener("Frio");
    }

    public void PausarJuego()
    {
        Time.timeScale = 0f; // Detiene el tiempo del juego
        juegoPausado = true; // Marca el juego como pausado
        BotonContinuar.SetActive(true);
        BotonMenu.SetActive(true);
        BotonReiniciar.SetActive(true);        
    }

    public void ReanudarJuego()
    {
   
        Time.timeScale = 1f; // Reanuda el tiempo del juego
        juegoPausado = false; // Marca el juego como no pausado
        BotonContinuar.SetActive(false);
        BotonMenu.SetActive(false);
        BotonReiniciar.SetActive(false);

    }

    public void ReiniciarJuego()
    {
        // Obtiene el nombre de la escena actual
        string nombreEscenaActual = SceneManager.GetActiveScene().name;

        // Compara el nombre de la escena actual y carga la escena correspondiente
        if (nombreEscenaActual == "Nivel1")
        {
            SceneManager.LoadScene("Nivel1");
        }
        
        if (nombreEscenaActual == "Nivel2")
        {
            SceneManager.LoadScene("Nivel2");
        }

        if (nombreEscenaActual == "Nivel3")
        {
            SceneManager.LoadScene("Nivel3");
        }


    }

    public void SalirJuego()
    {
        // Este m�todo cierra la aplicaci�n
        Time.timeScale = 1f;
        Application.Quit();

        // Solo para asegurarnos de que el comando fue recibido
        Debug.Log("La aplicaci�n se cerrar�.");
    }
}
