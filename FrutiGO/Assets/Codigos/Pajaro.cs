using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pajaro : MonoBehaviour
{
    public Vector3 puntoA; // Punto inicial (A)
    public Vector3 puntoB; // Punto final (B)
    public float velocidadMovimiento = 2f; // Velocidad de movimiento del p�jaro
    private Vector3 destinoActual; // Destino actual del p�jaro

    [Header("TIEMPOS")]
    public float tiempoEspera = 5f; // Tiempo de espera en cada punto
    public float tiempoTranscurrido; // Tiempo transcurrido desde el �ltimo cambio de destino
    public bool comenzarEnA = true; // Variable booleana para seleccionar el punto inicial
    public bool Izquierda;
    private bool llegoDestino = false; // Variable para controlar si el p�jaro lleg� al destino


    // ROTACION
    protected Vector3 Escala;
    protected float X;

    // estado aturdido
    private float ContadorAturdido;

    // ANIMACIONES
    public bool Volando;
    public bool Aturdiendose;
    private Animator Anim;

    // Start se llama antes de la primera actualizaci�n del cuadro
    void Start()
    {
       
        tiempoTranscurrido = 0; // Asegurarse de que el p�jaro comience a moverse de inmediato
        Volando = false;
        Aturdiendose = false;
        Anim = GetComponent<Animator>();

        Escala = transform.localScale;
        X = Escala.x;

        if (comenzarEnA)
        {
            Izquierda = false;
            transform.position = puntoA; // Comienza en el punto A
            destinoActual = puntoB; // Primer destino es el punto B
        }
        else
        {
            Izquierda = true;
            transform.position = puntoB; // Comienza en el punto B
            destinoActual = puntoA; // Primer destino es el punto A
        }
    }

    void Update()
    {
        if (!Aturdiendose)
        {
            Volar();
        }

        Direccion();
        Estados();
        Anim.SetBool("Volar", Volando);
        Anim.SetBool("Aturdido", Aturdiendose); // Enlace
    }

    private void Estados()
    {

        if (ContadorAturdido > 0)
        {
            ContadorAturdido -= Time.deltaTime;
            Aturdiendose = true;
        }
        else
        {
            Aturdiendose = false;
        }
//
            if (Vector3.Distance(transform.position, destinoActual) > 0.1f)
            {
                Volando = true;
            }
            else
            {
                Volando = false;
            }
    
    }

    private void Volar()
    {
        // Mueve el p�jaro hacia el destino actual
        transform.position = Vector3.MoveTowards(transform.position, destinoActual, velocidadMovimiento * Time.deltaTime);

        // Si el p�jaro ha llegado al destino
        if (Vector3.Distance(transform.position, destinoActual) < 0.1f)
        {
            if (!llegoDestino) // Solo invierte si no ha llegado previamente
            {
                Izquierda = !Izquierda; // Invierte la direcci�n
                llegoDestino = true; // Marca que ha llegado al destino
            }

            // Incrementa el tiempo transcurrido
            tiempoTranscurrido += Time.deltaTime;

            // Si ha pasado el tiempo de espera, cambia el destino
            if (tiempoTranscurrido >= tiempoEspera)
            {
                // Cambia el destino al punto opuesto
                destinoActual = destinoActual == puntoA ? puntoB : puntoA;

                // Reinicia el tiempo transcurrido
                tiempoTranscurrido = 0f;

                llegoDestino = false; // Resetea la marca de destino para la pr�xima vez
            }
        }
        else
        {
            llegoDestino = false; // Resetea la marca si no est� en destino
        }
    }

    private void Direccion()
    {
        if (Izquierda)
        {
            transform.localScale = new Vector3(-X, Escala.y, Escala.z); // Cambia la direcci�n a la izquierda
        }
        else
        {
            transform.localScale = new Vector3(X, Escala.y, Escala.z); // Cambia la direcci�n a la derecha
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Si el p�jaro colisiona con un objeto con el tag "Sacudida"
        if (collision.gameObject.CompareTag("Sacudida"))
        {
            // Desactiva el objeto del p�jaro
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Verificar si el objeto colisionado tiene la etiqueta "Temblor"
        if (other.gameObject.CompareTag("Temblor"))
        {
            ContadorAturdido = 6;
        }
    }
}
