using System.Collections.Generic; 
using UnityEngine; 

public class ObjectPool : MonoBehaviour
{
    // Array de prefabs de objetos que se utilizar�n en la piscina
    public GameObject[] objetosPrefabs;
    // Cantidad inicial de cada objeto en la piscina
    public int cantidadInicial = 10;

    // Lista que act�a como la piscina de objetos
    private List<GameObject> pool;

    private void Awake()
    {
        // Inicializa la lista de la piscina de objetos
        pool = new List<GameObject>();

        // Llena la piscina con la cantidad inicial de cada prefab
        for (int i = 0; i < cantidadInicial; i++)
        {
            foreach (GameObject prefab in objetosPrefabs)
            {
                // Instancia el objeto
                GameObject obj = Instantiate(prefab);                                       
                // Desactiva el objeto para reutilizarlo m�s tarde
                obj.SetActive(false);
                // A�ade el objeto a la lista de la piscina
                pool.Add(obj);
            }
        }
    }

    public GameObject ObtenerObjetoAleatorio()
    {



        // Lista temporal para almacenar los objetos desactivados
        List<GameObject> objetosDesactivados = new List<GameObject>();

        // Recorre la piscina buscando objetos desactivados y los agrega a la lista temporal
        foreach (GameObject obj in pool)
        {
            if (!obj.activeInHierarchy)
            {
                objetosDesactivados.Add(obj);
            }
        }

        // Si hay objetos desactivados disponibles
        if (objetosDesactivados.Count > 0)
        {
            // Selecciona un objeto desactivado aleatorio de la lista temporal
            int indiceAleatorio = Random.Range(0, objetosDesactivados.Count);
            GameObject objetoSeleccionado = objetosDesactivados[indiceAleatorio];
            // Activa el objeto seleccionado y lo devuelve
            objetoSeleccionado.SetActive(true);
            return objetoSeleccionado;
        }

        // Si no hay objetos desactivados disponibles, instancia un nuevo objeto aleatorio
        int indicePrefabAleatorio = Random.Range(0, objetosPrefabs.Length);
        GameObject nuevoObj = Instantiate(objetosPrefabs[indicePrefabAleatorio]);
        // Activa el nuevo objeto
        nuevoObj.SetActive(true);
        // A�ade el nuevo objeto a la piscina
        pool.Add(nuevoObj);
        // Devuelve el nuevo objeto
        return nuevoObj;
    }

}

