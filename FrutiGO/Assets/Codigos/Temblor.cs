using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temblor : MonoBehaviour


{
    public float TiempoTemblor;
    public GameObject TrigerTemblor;



    void Start()
    {

    }


    void Update()
    {
        TiempoTemblor = Mathf.Clamp(TiempoTemblor, 0, 1);

        if (TiempoTemblor > 0)
        {
            // Cambiar el tag del GameObject a "Temblor"
            gameObject.tag = "Temblor";
            TrigerTemblor.SetActive(true);
           
            TiempoTemblor -= Time.deltaTime;
        }

        if (TiempoTemblor <= 0)
        {
            gameObject.tag = "Pared";
            TrigerTemblor.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Si el jugador colisiona con el suelo, permite saltar de nuevo
        if (collision.gameObject.CompareTag("Jugador"))
        {
            TiempoTemblor = 1f;
        }
    }
}
