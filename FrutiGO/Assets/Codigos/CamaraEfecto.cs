using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraEfecto : MonoBehaviour
{
    // Funci�n que inicia la vibraci�n de la c�mara
    public IEnumerator Vibrar(float duracion, float magnitud)
    {
        // Guarda la posici�n original de la c�mara
        Vector3 posicionOriginal = transform.localPosition;
        float tiempoTranscurrido = 0.0f;

        // Mientras no se haya completado la duraci�n de la vibraci�n
        while (tiempoTranscurrido < duracion)
        {
            // Genera desplazamientos aleatorios en los ejes X e Y
            float desplazamientoX = Random.Range(-0.50f, 0.50f) * magnitud;
            float desplazamientoY =  posicionOriginal.y;

            // Aplica el desplazamiento a la posici�n de la c�mara
            transform.localPosition = new Vector3(desplazamientoX, desplazamientoY, posicionOriginal.z);

            // Incrementa el tiempo transcurrido
            tiempoTranscurrido += Time.deltaTime;

            // Espera al siguiente frame antes de continuar
            yield return null;
        }

        // Restaura la posici�n original de la c�mara
        transform.localPosition = posicionOriginal;
    }
}