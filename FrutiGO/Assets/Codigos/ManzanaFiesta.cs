using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManzanaFiesta : MonoBehaviour
{
    public float frecuencia = 1f; // Frecuencia de la pulsación
    public float amplitud = 0.1f; // Amplitud de la pulsación
    private Vector3 escalaInicial; // Escala inicial del objeto

    void Start()
    {
        // Guardar la escala inicial del objeto
        escalaInicial = transform.localScale;
    }

    void Update()
    {
        // Calcular la escala en función del tiempo, la frecuencia y la amplitud
        float escala = 1 + Mathf.Sin(Time.time * frecuencia) * amplitud;

        // Aplicar la nueva escala al objeto
        transform.localScale = escalaInicial * escala;
    }
}