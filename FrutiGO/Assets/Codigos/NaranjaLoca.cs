using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaranjaLoca : MonoBehaviour
{
    private SpriteRenderer spriteRenderer; // Referencia al componente SpriteRenderer
    public float velocidadCambio = 1f; // Velocidad del cambio de color

    void Start()
    {
        // Obtener el componente SpriteRenderer adjunto al mismo GameObject
        spriteRenderer = GetComponent<SpriteRenderer>();

        // Verificar si el componente SpriteRenderer se obtuvo correctamente
        if (spriteRenderer == null)
        {
            Debug.LogError("No se encontr� el componente SpriteRenderer en este GameObject.");
        }
    }

    void Update()
    {
        // Calcular el color en el espacio de color HSV
        float t = Time.time * velocidadCambio % 1; // Tiempo modificado por la velocidad
        Color color = Color.HSVToRGB(t, 1, 1); // Convertir el valor de t en un color RGB

        // Aplicar el color al SpriteRenderer
        if (spriteRenderer != null)
        {
            spriteRenderer.color = color;
        }
    }
}
