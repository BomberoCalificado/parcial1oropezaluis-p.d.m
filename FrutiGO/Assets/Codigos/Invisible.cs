using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invisible : MonoBehaviour
{
    [Header("FUERZA DE GRAVEDAD CONTRARIA")]
    private Rigidbody2D rb;
    public float fuerzaContraria = 3.0f;
    public float velocidadMaxima = 5.0f;
    [Space(5)]

    [Header("EXPLOSION")]
    private bool audioReproducido = false; // Variable para controlar si el audio ya se ha reproducido
    public ParticleSystem ExplosionEfecto;
  

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();       

        // Buscar el objeto hijo llamado "Explosion" y obtener su componente ParticleSystem
        Transform explosionTransform = transform.Find("Explosion");
        if (explosionTransform != null)
        {
            ExplosionEfecto = explosionTransform.GetComponent<ParticleSystem>();
            if (ExplosionEfecto == null)
            {
                Debug.LogWarning("El objeto hijo 'Explosion' no tiene un componente ParticleSystem.");
            }
        }
        else
        {
            Debug.LogWarning("No se encontr� el objeto hijo 'Explosion'.");
        }
    }


    void OnEnable()
    {
        // Reiniciar la variable cuando el objeto se active nuevamente
        audioReproducido = false;
        
    }
    // Update is called once per frame
    void Update()
    {
        GravedadContraria();
    }

    public void GravedadContraria()
    {
        // Aplicar una fuerza contraria a la gravedad
        rb.AddForce(Vector2.up * fuerzaContraria);

        // Limitar la velocidad m�xima de ca�da
        if (rb.velocity.y < -velocidadMaxima)
        {
            rb.velocity = new Vector2(rb.velocity.x, -velocidadMaxima);
        }
    }

    private IEnumerator DesactivarConRetardo()
    {
        if (!audioReproducido)
        {

            // Activar las part�culas y reproducir el sonido una vez
            Handheld.Vibrate();
            ExplosionEfecto.Play();
            GestorAudio.instancia.Reproducir("Pared");
            audioReproducido = true; // Marcar el audio como reproducido

        }

        // Esperar 2 segundos
        yield return new WaitForSeconds(0.55f);

        // Desactivar el objeto
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Si el jugador colisiona con el suelo, permite saltar de nuevo
        if (collision.gameObject.CompareTag("Suelo"))
        {
            // Iniciar la corrutina para desactivar el objeto despu�s de un retardo
            GestorAudio.instancia.Reproducir("Vidrio");
            StartCoroutine(DesactivarConRetardo());
        }

        // Si el jugador colisiona con el suelo, permite saltar de nuevo
        if (collision.gameObject.CompareTag("Jugador"))
        {
            // Iniciar la corrutina para desactivar el objeto despu�s de un retardo
            GestorAudio.instancia.Reproducir("Vidrio");
            StartCoroutine(DesactivarConRetardo());

        }

        // Si el jugador colisiona con el suelo, permite saltar de nuevo
        if (collision.gameObject.CompareTag("Muerte"))
        {
            // Iniciar la corrutina para desactivar el objeto despu�s de un retardo
            GestorAudio.instancia.Reproducir("Vidrio");
            StartCoroutine(DesactivarConRetardo());

        }
    }
}
