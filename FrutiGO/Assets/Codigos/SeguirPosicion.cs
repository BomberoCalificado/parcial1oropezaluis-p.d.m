using UnityEngine;
using UnityEngine.Rendering.Universal;

public class SeguirPosicion : MonoBehaviour
{
    public GameObject Objetivo;
    private Light2D luz2D; // Referencia al componente Light2D
    public float intensidadActivada = 20f;
    public float intensidadDesactivada = 0f;

    // Start is called before the first frame update
    void Start()
    {
        // Obtiene la referencia al componente Light2D
        luz2D = GetComponent<Light2D>();

        if (luz2D == null)
        {
            Debug.LogError("No se encontr� el componente Light2D en el GameObject.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Objetivo.activeInHierarchy)  // Verifica si el objetivo est� activado
        {
            // Sigue la posici�n del objetivo
            gameObject.transform.position = Objetivo.transform.position;

            // Ajusta la intensidad de la luz
            if (luz2D != null)
            {
                luz2D.intensity = intensidadActivada;
            }
        }
        else
        {
            // Ajusta la intensidad de la luz
            if (luz2D != null)
            {
                luz2D.intensity = intensidadDesactivada;
            }
        }
    }
}