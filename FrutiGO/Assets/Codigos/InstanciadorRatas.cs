using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorRatas : MonoBehaviour
{
    public GameObject Luz;

    // Lista de puntos de aparición donde los objetos pueden aparecer
    public Transform puntoAparicion;

    // Tiempo en segundos entre la aparición de cada objeto
    public float tiempoEntreInstancias = 8f;

    // Referencia a la piscina de objetos para obtener objetos de ella
    public ObjetPoolSimple objectPool;

    private void Start()
    {
        // Inicia la corrutina para instanciar objetos de manera aleatoria
        StartCoroutine(InstanciarObjetosAleatoriamente());
    }

    private IEnumerator InstanciarObjetosAleatoriamente()
    {
        // Bucle infinito para instanciar objetos continuamente
        while (true)
        {
            // Espera durante el tiempo especificado entre instancias
            yield return new WaitForSeconds(tiempoEntreInstancias);
            if (ControlNivel.Ganar == false && ControlNivel.Perder == false)
            {
                // Obtiene un objeto aleatorio de la piscina de objetos
                GameObject objeto = objectPool.ObtenerObjetoAleatorio();

                // Coloca el objeto en la posición del punto de aparición seleccionado aleatoriamente
                Luz.transform.position = puntoAparicion.position;
                objeto.transform.position = puntoAparicion.position;
            }

        }
    }
}
