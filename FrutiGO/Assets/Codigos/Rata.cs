using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Rata : MonoBehaviour
{
    // Variables p�blicas para ajustar desde el inspector
    [SerializeField] private float distanciaMaxima = 5f; // Distancia m�xima en el eje Y
    [SerializeField] private float velocidadBajada = 2f; // Velocidad de movimiento en el eje Y
    [SerializeField] private float velocidadHorizontal = 2f; // Velocidad de movimiento en el eje X

    // Variables privadas para controlar el estado del movimiento
    private bool Bajando = true; // Indica si la rata est� subiendo
    private bool moviendoHorizontalmente = false; // Indica si la rata se mueve horizontalmente
    private float posicionInicialY; // Guarda la posici�n inicial en el eje Y
    public bool Izquierda;


    public bool IzquierdaOriginal;

    // ROTACION
    protected Vector3 Escala;
    protected float X;

    // ANIMACIONES
    private Animator Anim;
    
    void OnEnable()
    {
     
       Izquierda = IzquierdaOriginal;
    }

    void Start()
    {
     

        // Guarda la posici�n inicial en el eje Y
        posicionInicialY = transform.position.y;
        Escala = transform.localScale;
        X = Escala.x;

        Anim = GetComponent<Animator>();
    }

   
    void Update()
    {
        
        if (Izquierda == false)
        {
            transform.localScale = new Vector3(-X, Escala.y, Escala.z);
        }

        // Si la rata est� bajando
        if (Bajando)
        {
            Bajar();
        }

        // Si la rata ha llegado a la distancia m�xima y debe moverse horizontalmente
        else if (moviendoHorizontalmente)
        {
            MoverHorizontalmente();
        }

        Anim.SetBool("Correr", moviendoHorizontalmente);   // enlace
    }

    // M�todo para mover la rata hacia abajo
    void Bajar()
    {
        // Mueve la rata hacia arriba
        transform.Translate(Vector2.down * velocidadBajada * Time.deltaTime);

        // Si la rata ha llegado a la distancia m�xima
        if (transform.position.y <= posicionInicialY + distanciaMaxima)
        {
            // Cambia el estado para empezar a moverse horizontalmente
            Bajando = false;
            moviendoHorizontalmente = true;
        }
    }

    // M�todo para mover la rata en el eje X
    void MoverHorizontalmente()
    {
        // Determina la direcci�n horizontal (positiva o negativa) 
        if (Izquierda == true)
        {
             // Mueve la rata en la direcci�n horizontal izquierda
            transform.Translate(Vector2.left * velocidadHorizontal * Time.deltaTime);

        }

        else
        {           
            // Mueve la rata en la direcci�n horizontal derecha
            transform.Translate(Vector2.right * velocidadHorizontal * Time.deltaTime);
          
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
  
        if (collision.gameObject.CompareTag("Temblor"))
        {

            Bajando = true;
            moviendoHorizontalmente = false;
            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("Jugador"))
        {
            Bajando = true;
            moviendoHorizontalmente = false;
            gameObject.SetActive(false);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        // Verificar si el objeto colisionado tiene la etiqueta "Temblor"
        if (other.gameObject.CompareTag("Temblor"))
        {
            // Cambiar los estados seg�n la colisi�n
            Bajando = true;
            moviendoHorizontalmente = false;
            gameObject.SetActive(false);
        }
    }
}
