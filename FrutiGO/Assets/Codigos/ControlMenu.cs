using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenu : MonoBehaviour
{
    private void Start()
    {
        PararAudioEventos();
    }


    public void Jugar (){

        SceneManager.LoadScene("Niveles");

    }
    public void Creditos()
    {
        // SceneManager.LoadScene("Creditos");
    }
    public void SalirDelJuego()
    {
        // Si estamos en el editor de Unity

        Application.Quit();

    }

    public void PararAudioEventos()
    {
        GestorAudio.instancia.Detener("Musica");
        GestorAudio.instancia.Detener("FiestaPuntos");
        GestorAudio.instancia.Detener("Horror");
        GestorAudio.instancia.Detener("ManzanaFiesta");
        GestorAudio.instancia.Detener("Frio");
    }
    public void Nivel1()
    {

        SceneManager.LoadScene("Nivel1");

    }
    public void Nivel2()
    {

        SceneManager.LoadScene("Nivel2");

    }
    public void Nivel3()
    {

        SceneManager.LoadScene("Nivel3");

    }

}
