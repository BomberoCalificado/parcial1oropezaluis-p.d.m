using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciadorFrutas : MonoBehaviour
{
    public GameObject Luz;

    // Lista de puntos de aparición donde los objetos pueden aparecer
    public Transform[] puntosAparicion;

    // Tiempo en segundos entre la aparición de cada objeto
    public float tiempoEntreInstancias = 1f;

    // Referencia a la piscina de objetos para obtener objetos de ella
    public ObjectPool objectPool;
    private  ObjectPool objectPoolNormal;
    public ObjectPool FiestaManzanas;

    public Jugador Mijugador;

    private void Start()
    {
        //Mijugador = new Jugador();
        objectPoolNormal = objectPool; // guardamos el original
        // Inicia la corrutina para instanciar objetos de manera aleatoria
        StartCoroutine(InstanciarObjetosAleatoriamente());
    }

    private void Update()
    {
        if (Mijugador.IndicadorEstado == 5)
        {
            objectPool = FiestaManzanas;
            tiempoEntreInstancias = 0.50f;
        }
        else
        {
            objectPool = objectPoolNormal;
            tiempoEntreInstancias = 1;
        }
    }

    private IEnumerator InstanciarObjetosAleatoriamente()
    {
        // Bucle infinito para instanciar objetos continuamente
        while (true)
        {
            // Espera durante el tiempo especificado entre instancias
            yield return new WaitForSeconds(tiempoEntreInstancias);
            if (ControlNivel.Ganar == false && ControlNivel.Perder == false)
            {
                // Selecciona un índice aleatorio de la lista de puntos de aparición
                int indicePuntoAleatorio = Random.Range(0, puntosAparicion.Length);

                // Obtiene un objeto aleatorio de la piscina de objetos
                GameObject objeto = objectPool.ObtenerObjetoAleatorio();

                // Coloca el objeto en la posición del punto de aparición seleccionado aleatoriamente
                Luz.transform.position = puntosAparicion[indicePuntoAleatorio].position;
                objeto.transform.position = puntosAparicion[indicePuntoAleatorio].position;
            }
                
        }
    }
}
