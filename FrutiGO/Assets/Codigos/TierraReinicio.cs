using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class TierraReinicio : MonoBehaviour
{
    public GameObject TierraObjeto;
    public Tierra Mitierra;
    public float ContadorReinicio;



    void Start()
    {
        ContadorReinicio = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        math.clamp(ContadorReinicio,0,10);

        if(Mitierra.Vida <= 0)
        {
            ContadorReinicio = ContadorReinicio + Time.deltaTime;
        }

        if (ContadorReinicio >= 10)
        {

           TierraObjeto.SetActive(true);
           ContadorReinicio = 0;
        }
    }
}
