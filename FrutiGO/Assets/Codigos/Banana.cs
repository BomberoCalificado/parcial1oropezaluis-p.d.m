using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Banana : MonoBehaviour
{
    // Referencia a la piscina de objetos para obtener objetos de ella
   [SerializeField] private ObjectPool objectPool2;
    public GameObject Cascara;

    private Rigidbody2D rb;
    public float fuerzaContraria = 3.0f;
    public float velocidadMaxima = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        // Buscar y asignar el ObjectPool por su tag
        objectPool2 = GameObject.FindWithTag("CascarasBanana").GetComponent<ObjectPool>();
        gameObject.SetActive(false);
    }


    // Update is called once per frame
    void Update()
    {
        
    }


    void FixedUpdate()
    {
        // Aplicar una fuerza contraria a la gravedad
        rb.AddForce(Vector2.up * fuerzaContraria);

        // Limitar la velocidad m�xima de ca�da
        if (rb.velocity.y < -velocidadMaxima)
        {
            rb.velocity = new Vector2(rb.velocity.x, -velocidadMaxima);
        }
    }

    private void LLamarObjeto()
    {

        { 
            // Obtiene un objeto aleatorio de la piscina de objetos
            GameObject objeto = objectPool2.ObtenerObjetoAleatorio();

            // Coloca el objeto en la posici�n del punto de aparici�n seleccionado 
            objeto.transform.position = gameObject.transform.position;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Jugador"))
        {
            gameObject.SetActive(false);
        }
        if (collision.gameObject.CompareTag("Pajaro"))
        {
            gameObject.SetActive(false);
        }

        if (collision.gameObject.CompareTag("Suelo"))
        {
            // llamamos el objeto cascara de banana y lo colocamos en esta posicion
            LLamarObjeto(); 
           
            gameObject.SetActive(false); //desactivamos la banana
        }

      
    }
}
